﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Wonn : MonoBehaviour
{
    public AudioSource source;
    public AudioClip click;

    public void PlayAgain()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void Quit()
    {
        Debug.Log("QUIT!");
        Application.Quit();
    }



    public void Onclick()
    {
        source.PlayOneShot(click);
    }
}
