﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Try::Start()
extern void Try_Start_m77D9F45762D9F92B629EA62238C835B4CB6E78F8 ();
// 0x00000002 System.Void Try::Update()
extern void Try_Update_m0A0CDCEC34F921E403FAE59FEC4F74FAB60B75A2 ();
// 0x00000003 System.Collections.IEnumerator Try::GameOver()
extern void Try_GameOver_m63B4882E6F5350C69D3A60375E0F37A9D886FCC8 ();
// 0x00000004 System.Void Try::.ctor()
extern void Try__ctor_m17D029E7B5721BFF27701E579B38EA87F6EBBACF ();
// 0x00000005 System.Void SurfaceChecker::Start()
extern void SurfaceChecker_Start_m7400B2EE1A899C91E301778ABD13C1DC8028BFE7 ();
// 0x00000006 System.Void SurfaceChecker::OnGUI()
extern void SurfaceChecker_OnGUI_mBBFBD99AB32C1C9D01F60AF73AC3A1A9AE7F99B5 ();
// 0x00000007 System.Void SurfaceChecker::Update()
extern void SurfaceChecker_Update_m94C00EF9477B72AD1B162B3F6ADAEC80AD57E496 ();
// 0x00000008 System.Void SurfaceChecker::UpdateTargetIndicator()
extern void SurfaceChecker_UpdateTargetIndicator_mCAB90E3DFA3B954FB764ECA1C599EF1CCA7FD3CA ();
// 0x00000009 System.Void SurfaceChecker::UpdatePlacementPose()
extern void SurfaceChecker_UpdatePlacementPose_m5530A5B7DD542CE0783ECEDDF5CCB60FCDA96CBC ();
// 0x0000000A System.Void SurfaceChecker::.ctor()
extern void SurfaceChecker__ctor_mBCC8B256BABABC33A19219B00A57100E44531F11 ();
// 0x0000000B UnityEngine.Material UnityStandardAssets.ImageEffects.Antialiasing::CurrentAAMaterial()
extern void Antialiasing_CurrentAAMaterial_mDCAB4F1E05EDD77DE4019689E9B8E0D8C83A8C54 ();
// 0x0000000C System.Boolean UnityStandardAssets.ImageEffects.Antialiasing::CheckResources()
extern void Antialiasing_CheckResources_m498B9EE12AFAD27688FAC7173EEB43F81017FB88 ();
// 0x0000000D System.Void UnityStandardAssets.ImageEffects.Antialiasing::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Antialiasing_OnRenderImage_mA976E0852A74E97648A82B6BD10E66ACC1AF7FD4 ();
// 0x0000000E System.Void UnityStandardAssets.ImageEffects.Antialiasing::.ctor()
extern void Antialiasing__ctor_m55C3D8A38ACC338A1FE308C1DE62C63975924F84 ();
// 0x0000000F System.Boolean UnityStandardAssets.ImageEffects.Bloom::CheckResources()
extern void Bloom_CheckResources_m92106259EA26543B36D042032DE604E103FA6D68 ();
// 0x00000010 System.Void UnityStandardAssets.ImageEffects.Bloom::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Bloom_OnRenderImage_mB9448277B070C65684A4752B979638AF435DB106 ();
// 0x00000011 System.Void UnityStandardAssets.ImageEffects.Bloom::AddTo(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Bloom_AddTo_m2C7566CB79AD6CB39439004E35DE6F89C42621F4 ();
// 0x00000012 System.Void UnityStandardAssets.ImageEffects.Bloom::BlendFlares(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Bloom_BlendFlares_mBE0265621414D22718647708DE1471D19D17C9B9 ();
// 0x00000013 System.Void UnityStandardAssets.ImageEffects.Bloom::BrightFilter(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Bloom_BrightFilter_m0B93F00834C7B463AF74F9A5D755D2647A1F96A4 ();
// 0x00000014 System.Void UnityStandardAssets.ImageEffects.Bloom::BrightFilter(UnityEngine.Color,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Bloom_BrightFilter_m30BA24087FB56D24159743A86EE8153B6E08A576 ();
// 0x00000015 System.Void UnityStandardAssets.ImageEffects.Bloom::Vignette(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Bloom_Vignette_m05374CF6C79C9435CDB9915669D3E6CCCAE91783 ();
// 0x00000016 System.Void UnityStandardAssets.ImageEffects.Bloom::.ctor()
extern void Bloom__ctor_mDBE9F226A2C05547E468A1095EC0CC57DBCD6C27 ();
// 0x00000017 System.Boolean UnityStandardAssets.ImageEffects.BloomAndFlares::CheckResources()
extern void BloomAndFlares_CheckResources_m16DCC48C4E67BCB0BE3219A77CAA1454E6CEBAD9 ();
// 0x00000018 System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void BloomAndFlares_OnRenderImage_m801989E7A9037FC9EE826E2EB363EE42578EDC0A ();
// 0x00000019 System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::AddTo(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void BloomAndFlares_AddTo_m00E0AC64F23F18A9EF9D6CDF8BBE3C82ABABF853 ();
// 0x0000001A System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::BlendFlares(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void BloomAndFlares_BlendFlares_mE4C7FC30CA32888A348B4490305DC8A750DDF442 ();
// 0x0000001B System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::BrightFilter(System.Single,System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void BloomAndFlares_BrightFilter_mBA4DE2DFD592941DEBC1CF737EBA45F1E5093A15 ();
// 0x0000001C System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::Vignette(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void BloomAndFlares_Vignette_mCCAD05854B3AF95FCEFE32022170EEBE66D03766 ();
// 0x0000001D System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::.ctor()
extern void BloomAndFlares__ctor_mAB0F2AD4210DC5BCCDAD71FEE375DD4ACC48B7ED ();
// 0x0000001E System.Boolean UnityStandardAssets.ImageEffects.BloomOptimized::CheckResources()
extern void BloomOptimized_CheckResources_mD632E635CB25AD6B910137EACA4B47413782B297 ();
// 0x0000001F System.Void UnityStandardAssets.ImageEffects.BloomOptimized::OnDisable()
extern void BloomOptimized_OnDisable_m6C76582A668E9F0AD13481E54FF47AFBF5FE7EFE ();
// 0x00000020 System.Void UnityStandardAssets.ImageEffects.BloomOptimized::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void BloomOptimized_OnRenderImage_m8ACD073311ECEB136FF61084DD541125C69F2C42 ();
// 0x00000021 System.Void UnityStandardAssets.ImageEffects.BloomOptimized::.ctor()
extern void BloomOptimized__ctor_m9A393CAC94F698AC6DFD0407C439C885DF249189 ();
// 0x00000022 UnityEngine.Material UnityStandardAssets.ImageEffects.Blur::get_material()
extern void Blur_get_material_mC339DD88FD2BC429680CA2F25C8BED5F80735B79 ();
// 0x00000023 System.Void UnityStandardAssets.ImageEffects.Blur::OnDisable()
extern void Blur_OnDisable_m709199A9983BE2C2283D6DEF0980653036951F10 ();
// 0x00000024 System.Void UnityStandardAssets.ImageEffects.Blur::Start()
extern void Blur_Start_m6C503845B602898F68A18E5A0D6D7712FD818586 ();
// 0x00000025 System.Void UnityStandardAssets.ImageEffects.Blur::FourTapCone(UnityEngine.RenderTexture,UnityEngine.RenderTexture,System.Int32)
extern void Blur_FourTapCone_m651F9E47B8AB1958902785D9507F6F46130AC8B3 ();
// 0x00000026 System.Void UnityStandardAssets.ImageEffects.Blur::DownSample4x(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Blur_DownSample4x_mF95A54B819BAC3B7140F3CAE39EC1F7B0FBF4776 ();
// 0x00000027 System.Void UnityStandardAssets.ImageEffects.Blur::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Blur_OnRenderImage_m371A7A264E94739EFEED8EE1CB1CFC9F9F04DEC7 ();
// 0x00000028 System.Void UnityStandardAssets.ImageEffects.Blur::.ctor()
extern void Blur__ctor_m85C8C095B8C3B506E20B36B7E1838B4B06AC93E0 ();
// 0x00000029 System.Void UnityStandardAssets.ImageEffects.Blur::.cctor()
extern void Blur__cctor_mD80C6694F31CC7E5A18C9A8ABC9209782F309189 ();
// 0x0000002A System.Boolean UnityStandardAssets.ImageEffects.BlurOptimized::CheckResources()
extern void BlurOptimized_CheckResources_m33D49532FE335BEC1B842D4C1016473B1EBE7CB1 ();
// 0x0000002B System.Void UnityStandardAssets.ImageEffects.BlurOptimized::OnDisable()
extern void BlurOptimized_OnDisable_m2FFA9FFB9791D797603B589E7C9F9A164A1B5BE3 ();
// 0x0000002C System.Void UnityStandardAssets.ImageEffects.BlurOptimized::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void BlurOptimized_OnRenderImage_m1C441E7D77F6D3B77C9C4BD49D14855AD352B3A9 ();
// 0x0000002D System.Void UnityStandardAssets.ImageEffects.BlurOptimized::.ctor()
extern void BlurOptimized__ctor_mF83F3B51DD987C09E5BE74EC65795FC89B51F019 ();
// 0x0000002E System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::CalculateViewProjection()
extern void CameraMotionBlur_CalculateViewProjection_mACC204367238A287CC7A38C01394C82CE5EDED87 ();
// 0x0000002F System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::Start()
extern void CameraMotionBlur_Start_m97790DE300FC791125EA136645CE2D77EB73F4AA ();
// 0x00000030 System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::OnEnable()
extern void CameraMotionBlur_OnEnable_m08997525FECCD04035EF42F33919941BE139DC03 ();
// 0x00000031 System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::OnDisable()
extern void CameraMotionBlur_OnDisable_m1DF62D2CAB4995F38C23B1966F5EF47C7340CF08 ();
// 0x00000032 System.Boolean UnityStandardAssets.ImageEffects.CameraMotionBlur::CheckResources()
extern void CameraMotionBlur_CheckResources_m8D026DBB9258C285D2FF092F44147C337B245025 ();
// 0x00000033 System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void CameraMotionBlur_OnRenderImage_m825CD100D4C2A4262BFB96E1461748B49A1DAF5D ();
// 0x00000034 System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::Remember()
extern void CameraMotionBlur_Remember_mF7924AB7BF552DA1568B8671E7E7DAE57073EAA8 ();
// 0x00000035 UnityEngine.Camera UnityStandardAssets.ImageEffects.CameraMotionBlur::GetTmpCam()
extern void CameraMotionBlur_GetTmpCam_mFC1B30B16B7CD31C659E7660DF7928E9183ED7E2 ();
// 0x00000036 System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::StartFrame()
extern void CameraMotionBlur_StartFrame_m7D9B86914F003077FBD6BA88D5488BA163EBFFDB ();
// 0x00000037 System.Int32 UnityStandardAssets.ImageEffects.CameraMotionBlur::divRoundUp(System.Int32,System.Int32)
extern void CameraMotionBlur_divRoundUp_m920A99D3CFC242852027F0560968681FA722CBDD ();
// 0x00000038 System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::.ctor()
extern void CameraMotionBlur__ctor_m63EB59EE479795A61C8BDC7817338590FC944017 ();
// 0x00000039 System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::.cctor()
extern void CameraMotionBlur__cctor_m72319853F4B6038AED29E727CABADA3F81A83DA3 ();
// 0x0000003A System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::Start()
extern void ColorCorrectionCurves_Start_m96C795F6E1B3E3F2969590C1842338D9DE4336CC ();
// 0x0000003B System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::Awake()
extern void ColorCorrectionCurves_Awake_m611F8DC335D42F19A9F5BE775AE622069C80F8F6 ();
// 0x0000003C System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionCurves::CheckResources()
extern void ColorCorrectionCurves_CheckResources_mE8B24D1F01C6E1EA879816FBB17C203CC33FB775 ();
// 0x0000003D System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::UpdateParameters()
extern void ColorCorrectionCurves_UpdateParameters_m4849CC559E048FD7E3E7FAF83DBBDFB019D54415 ();
// 0x0000003E System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::UpdateTextures()
extern void ColorCorrectionCurves_UpdateTextures_m05A13699BF3A502399C23D86C00782D8033BD7BC ();
// 0x0000003F System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ColorCorrectionCurves_OnRenderImage_m1B8C7B3ADBE01FA66853467EB0B07A563A5F8352 ();
// 0x00000040 System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::.ctor()
extern void ColorCorrectionCurves__ctor_m68722ED5CF2EB526B5F02A93D233B9D95F2C6D97 ();
// 0x00000041 System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionLookup::CheckResources()
extern void ColorCorrectionLookup_CheckResources_m667A5A869CC0206C1E33640EC4E76A5E03078721 ();
// 0x00000042 System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::OnDisable()
extern void ColorCorrectionLookup_OnDisable_mF1F9FD81B997F5C64C76D6EA4D9FFAAD778752DC ();
// 0x00000043 System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::OnDestroy()
extern void ColorCorrectionLookup_OnDestroy_mB168904FCBE7B6038896284D55FFC7C94A1BA182 ();
// 0x00000044 System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::SetIdentityLut()
extern void ColorCorrectionLookup_SetIdentityLut_mDEB155C0810472446CB5DB837AEB24294F981C09 ();
// 0x00000045 System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionLookup::ValidDimensions(UnityEngine.Texture2D)
extern void ColorCorrectionLookup_ValidDimensions_mFBAEDC77ED3A2937C618CBE36B374A213523147E ();
// 0x00000046 System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::Convert(UnityEngine.Texture2D,System.String)
extern void ColorCorrectionLookup_Convert_mBEE45AD4A6BFCD6B311016298F9D5B6F2B6FEFC2 ();
// 0x00000047 System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ColorCorrectionLookup_OnRenderImage_m9CCFEE4FD27332D235C59239FE2A286728CC9A52 ();
// 0x00000048 System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::.ctor()
extern void ColorCorrectionLookup__ctor_m0DFF254050E4CEB608FCA1462A9587A227973D9F ();
// 0x00000049 System.Void UnityStandardAssets.ImageEffects.ColorCorrectionRamp::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ColorCorrectionRamp_OnRenderImage_m525E7EC26C5C858DE4A14C7BB82A743343D94B84 ();
// 0x0000004A System.Void UnityStandardAssets.ImageEffects.ColorCorrectionRamp::.ctor()
extern void ColorCorrectionRamp__ctor_m3943A74E178F836266D12D363F943566C0AF2779 ();
// 0x0000004B System.Boolean UnityStandardAssets.ImageEffects.ContrastEnhance::CheckResources()
extern void ContrastEnhance_CheckResources_m285591510F4D995B7BD926A306BCD69485A26EAD ();
// 0x0000004C System.Void UnityStandardAssets.ImageEffects.ContrastEnhance::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ContrastEnhance_OnRenderImage_m194287EACB23A9B4561D8B47D9BE2B92B33BE38E ();
// 0x0000004D System.Void UnityStandardAssets.ImageEffects.ContrastEnhance::.ctor()
extern void ContrastEnhance__ctor_m03223D280A37A44AF64FB8D5368B8A652BE1DE25 ();
// 0x0000004E UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::get_materialLum()
extern void ContrastStretch_get_materialLum_m3B94D8370783820BF61ADEA97169CD858D12BD0A ();
// 0x0000004F UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::get_materialReduce()
extern void ContrastStretch_get_materialReduce_mFFAEB3EC39637918599727612C933A794D5B482F ();
// 0x00000050 UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::get_materialAdapt()
extern void ContrastStretch_get_materialAdapt_m95A164030016587300035F3DE58087F5F24ADF50 ();
// 0x00000051 UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::get_materialApply()
extern void ContrastStretch_get_materialApply_mD9802FD06F513EC1BD1D20F20E52242150EAFEEC ();
// 0x00000052 System.Void UnityStandardAssets.ImageEffects.ContrastStretch::Start()
extern void ContrastStretch_Start_mE367825F381B6813C8766323829735A6279597CD ();
// 0x00000053 System.Void UnityStandardAssets.ImageEffects.ContrastStretch::OnEnable()
extern void ContrastStretch_OnEnable_mDD7FECD219EAC6A7075290DF68F21862388E4718 ();
// 0x00000054 System.Void UnityStandardAssets.ImageEffects.ContrastStretch::OnDisable()
extern void ContrastStretch_OnDisable_mD742FF9882E9D9060B8967269C87F9F978D059F5 ();
// 0x00000055 System.Void UnityStandardAssets.ImageEffects.ContrastStretch::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ContrastStretch_OnRenderImage_m1CF0C6BACB625EE274D813A9181925A3E31FC649 ();
// 0x00000056 System.Void UnityStandardAssets.ImageEffects.ContrastStretch::CalculateAdaptation(UnityEngine.Texture)
extern void ContrastStretch_CalculateAdaptation_m037490E256613C7D4BA7E209A2632CCE403B2B12 ();
// 0x00000057 System.Void UnityStandardAssets.ImageEffects.ContrastStretch::.ctor()
extern void ContrastStretch__ctor_m59166DE11011E5DBBD366B2B594F67198508EC07 ();
// 0x00000058 System.Boolean UnityStandardAssets.ImageEffects.CreaseShading::CheckResources()
extern void CreaseShading_CheckResources_m75335C51F99C80FB15EC8CA2CE8C17D5D7CCDCE0 ();
// 0x00000059 System.Void UnityStandardAssets.ImageEffects.CreaseShading::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void CreaseShading_OnRenderImage_mA734C73D6E2BD430F9156A34FEFF7C3E0EC261B2 ();
// 0x0000005A System.Void UnityStandardAssets.ImageEffects.CreaseShading::.ctor()
extern void CreaseShading__ctor_m5F1EA0C9E5F16FBDD152AE723066DCCCD7513906 ();
// 0x0000005B System.Boolean UnityStandardAssets.ImageEffects.DepthOfField::CheckResources()
extern void DepthOfField_CheckResources_m99902E019852BA913338849C829238FE94723076 ();
// 0x0000005C System.Void UnityStandardAssets.ImageEffects.DepthOfField::OnEnable()
extern void DepthOfField_OnEnable_m4CCE526BA0FC56B2B74F88640DF4A8CBC556637D ();
// 0x0000005D System.Void UnityStandardAssets.ImageEffects.DepthOfField::OnDisable()
extern void DepthOfField_OnDisable_mDCFFA4DC1A2119AC21F2287D500869B78A570291 ();
// 0x0000005E System.Void UnityStandardAssets.ImageEffects.DepthOfField::ReleaseComputeResources()
extern void DepthOfField_ReleaseComputeResources_mE9C0CFAA5296947EE0FD7F2EF077DAFAD304C969 ();
// 0x0000005F System.Void UnityStandardAssets.ImageEffects.DepthOfField::CreateComputeResources()
extern void DepthOfField_CreateComputeResources_mE8EFBE513F52E734A7EF32FCDFB9CFE8995577DD ();
// 0x00000060 System.Single UnityStandardAssets.ImageEffects.DepthOfField::FocalDistance01(System.Single)
extern void DepthOfField_FocalDistance01_m7C83CD9C716FD024B93ADDF8104C3FB7155C35DD ();
// 0x00000061 System.Void UnityStandardAssets.ImageEffects.DepthOfField::WriteCoc(UnityEngine.RenderTexture,System.Boolean)
extern void DepthOfField_WriteCoc_mD5EEEB880C4EBFF74E2512EC41FCBF2DE09E1AA8 ();
// 0x00000062 System.Void UnityStandardAssets.ImageEffects.DepthOfField::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void DepthOfField_OnRenderImage_mCCAF5027A0E4720B7EB307F6775FD481B2882A61 ();
// 0x00000063 System.Void UnityStandardAssets.ImageEffects.DepthOfField::.ctor()
extern void DepthOfField__ctor_mAAD3FC97724C123B194DE9AB78F1534C6E41D8C9 ();
// 0x00000064 System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::CreateMaterials()
extern void DepthOfFieldDeprecated_CreateMaterials_mECB51EE5BD0A165CD7857C928FDE637F592C839C ();
// 0x00000065 System.Boolean UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::CheckResources()
extern void DepthOfFieldDeprecated_CheckResources_mAF4DCDB80B5DE79A0BFD6B487F6A94CA9E20942C ();
// 0x00000066 System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::OnDisable()
extern void DepthOfFieldDeprecated_OnDisable_mE788E8B945FEBAB0D4B976595FEBD2A0615CA066 ();
// 0x00000067 System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::OnEnable()
extern void DepthOfFieldDeprecated_OnEnable_mA937196855E09E213A14AC82ECD6BBA36204B6A3 ();
// 0x00000068 System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::FocalDistance01(System.Single)
extern void DepthOfFieldDeprecated_FocalDistance01_m813C7BA00F70EE2499BA9FF010F50315C0358F78 ();
// 0x00000069 System.Int32 UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::GetDividerBasedOnQuality()
extern void DepthOfFieldDeprecated_GetDividerBasedOnQuality_mAEA18B66A910E404DCB26D56A764001B526E0EDB ();
// 0x0000006A System.Int32 UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::GetLowResolutionDividerBasedOnQuality(System.Int32)
extern void DepthOfFieldDeprecated_GetLowResolutionDividerBasedOnQuality_m07162EC127851FF5B26F81A300F2628269A4C899 ();
// 0x0000006B System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void DepthOfFieldDeprecated_OnRenderImage_mB105D9596DD852280DB427C68FD74E2FB5C9D406 ();
// 0x0000006C System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::Blur(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated_DofBlurriness,System.Int32,System.Single)
extern void DepthOfFieldDeprecated_Blur_mAD0A3BA1246C5D4B728983DC1D6AE4379CA3343B ();
// 0x0000006D System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::BlurFg(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated_DofBlurriness,System.Int32,System.Single)
extern void DepthOfFieldDeprecated_BlurFg_mB89E6D910AB144124AA390F592555F333F6FF238 ();
// 0x0000006E System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::BlurHex(UnityEngine.RenderTexture,UnityEngine.RenderTexture,System.Int32,System.Single,UnityEngine.RenderTexture)
extern void DepthOfFieldDeprecated_BlurHex_m92FCA492A8C5B533079254B451E65CC838DCA5B7 ();
// 0x0000006F System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::Downsample(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void DepthOfFieldDeprecated_Downsample_mA34F5C964DF9DCA387288AD6860799AEF5ACE3C4 ();
// 0x00000070 System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::AddBokeh(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void DepthOfFieldDeprecated_AddBokeh_m17F24DBB48B9E3F821DB6D0E8AE5BA732FFD8A53 ();
// 0x00000071 System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::ReleaseTextures()
extern void DepthOfFieldDeprecated_ReleaseTextures_m2CB5AC6D29EB2CCB7480BAEB97F80A9046EFB819 ();
// 0x00000072 System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::AllocateTextures(System.Boolean,UnityEngine.RenderTexture,System.Int32,System.Int32)
extern void DepthOfFieldDeprecated_AllocateTextures_mD948CE56E5DEC1CF0E51120DF9EFB354321E30C0 ();
// 0x00000073 System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::.ctor()
extern void DepthOfFieldDeprecated__ctor_m797E5E012F35EC7DFE7E5A9CE8C5D7A99E4A077F ();
// 0x00000074 System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::.cctor()
extern void DepthOfFieldDeprecated__cctor_m7A951BF59CEAE1D7C1FA676A246A3D46B2A0A935 ();
// 0x00000075 System.Boolean UnityStandardAssets.ImageEffects.EdgeDetection::CheckResources()
extern void EdgeDetection_CheckResources_m38B9BC21E5FBA2716D9319443277216191BFAC98 ();
// 0x00000076 System.Void UnityStandardAssets.ImageEffects.EdgeDetection::Start()
extern void EdgeDetection_Start_m242E5FB7F37327A2447953EEBF59A85451D8BCCC ();
// 0x00000077 System.Void UnityStandardAssets.ImageEffects.EdgeDetection::SetCameraFlag()
extern void EdgeDetection_SetCameraFlag_m2C438A1B4D88F41D69D5038E9230AB99FF5FA41E ();
// 0x00000078 System.Void UnityStandardAssets.ImageEffects.EdgeDetection::OnEnable()
extern void EdgeDetection_OnEnable_mAD697941029C90030AC0CF02FB7EA0DBBED6887A ();
// 0x00000079 System.Void UnityStandardAssets.ImageEffects.EdgeDetection::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void EdgeDetection_OnRenderImage_m48F01278CA34F14B17A169715DAC376B3D3D7189 ();
// 0x0000007A System.Void UnityStandardAssets.ImageEffects.EdgeDetection::.ctor()
extern void EdgeDetection__ctor_m00052AE0C25B0BDD68C10929C7484FA85B857521 ();
// 0x0000007B System.Boolean UnityStandardAssets.ImageEffects.Fisheye::CheckResources()
extern void Fisheye_CheckResources_m72A552D5AF92C69A59DD94A406660283BBC4CFDD ();
// 0x0000007C System.Void UnityStandardAssets.ImageEffects.Fisheye::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Fisheye_OnRenderImage_m57F4D4A413BD705977FA0E5776B7C43B7908CCAF ();
// 0x0000007D System.Void UnityStandardAssets.ImageEffects.Fisheye::.ctor()
extern void Fisheye__ctor_mBC52733545C135CE081043C8ED60ECEAAD941193 ();
// 0x0000007E System.Boolean UnityStandardAssets.ImageEffects.GlobalFog::CheckResources()
extern void GlobalFog_CheckResources_m4D2FEA4A379EBA867C51D442AAC297F07D6C1D35 ();
// 0x0000007F System.Void UnityStandardAssets.ImageEffects.GlobalFog::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void GlobalFog_OnRenderImage_mE92A91E8A011F3522CBC70D6771BEC588C4B10DB ();
// 0x00000080 System.Void UnityStandardAssets.ImageEffects.GlobalFog::CustomGraphicsBlit(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32)
extern void GlobalFog_CustomGraphicsBlit_m4D20CB76C88420AF37C959A360BF2EB8BC34DF71 ();
// 0x00000081 System.Void UnityStandardAssets.ImageEffects.GlobalFog::.ctor()
extern void GlobalFog__ctor_m0A9DCB22E188DFE08579AD41764831F203F730FA ();
// 0x00000082 System.Void UnityStandardAssets.ImageEffects.Grayscale::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Grayscale_OnRenderImage_m8A512F145218A68A55C64B84B23E2131C269A24A ();
// 0x00000083 System.Void UnityStandardAssets.ImageEffects.Grayscale::.ctor()
extern void Grayscale__ctor_m73FB63226C982A7360275EDB1B7C9CBECFFD5A92 ();
// 0x00000084 System.Void UnityStandardAssets.ImageEffects.ImageEffectBase::Start()
extern void ImageEffectBase_Start_mA1BA9EAFE8C4DB8D60AF9214354FA12B518C670F ();
// 0x00000085 UnityEngine.Material UnityStandardAssets.ImageEffects.ImageEffectBase::get_material()
extern void ImageEffectBase_get_material_mBBECA2FA70056036BB0A8C8EE12C005B5F540F32 ();
// 0x00000086 System.Void UnityStandardAssets.ImageEffects.ImageEffectBase::OnDisable()
extern void ImageEffectBase_OnDisable_m660A3E17A229AEC9D91618C2CB632FEB5EB9428C ();
// 0x00000087 System.Void UnityStandardAssets.ImageEffects.ImageEffectBase::.ctor()
extern void ImageEffectBase__ctor_m4A9D6AD3B56675C66BDFD7814F74661E8B567FEE ();
// 0x00000088 System.Void UnityStandardAssets.ImageEffects.ImageEffects::RenderDistortion(UnityEngine.Material,UnityEngine.RenderTexture,UnityEngine.RenderTexture,System.Single,UnityEngine.Vector2,UnityEngine.Vector2)
extern void ImageEffects_RenderDistortion_mEAD2A6D8B211C9B7EBE054F227AC22FA525AE2DF ();
// 0x00000089 System.Void UnityStandardAssets.ImageEffects.ImageEffects::Blit(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ImageEffects_Blit_mA6422E3EF42C14DD0012423410D5E6C23681A2A8 ();
// 0x0000008A System.Void UnityStandardAssets.ImageEffects.ImageEffects::BlitWithMaterial(UnityEngine.Material,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ImageEffects_BlitWithMaterial_mEB735C49FC9EBE60B95667B5C116A2ED5997E7A7 ();
// 0x0000008B System.Void UnityStandardAssets.ImageEffects.ImageEffects::.ctor()
extern void ImageEffects__ctor_mE1A4398899DA8975E63366AB56CA498DE82E16BF ();
// 0x0000008C System.Void UnityStandardAssets.ImageEffects.MotionBlur::Start()
extern void MotionBlur_Start_m82B9C590D5312C265F3F59D6CF8A9F39CE46F0E4 ();
// 0x0000008D System.Void UnityStandardAssets.ImageEffects.MotionBlur::OnDisable()
extern void MotionBlur_OnDisable_m06EA52C7FCBF0FB355978CD5489FB0AC60F45DF3 ();
// 0x0000008E System.Void UnityStandardAssets.ImageEffects.MotionBlur::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void MotionBlur_OnRenderImage_m24D6DCD45A81C6B2EA33E700BEF2E4F485B31CD4 ();
// 0x0000008F System.Void UnityStandardAssets.ImageEffects.MotionBlur::.ctor()
extern void MotionBlur__ctor_m69F5FB6BF9026F8D10E338A3EA9F2C63C57CF605 ();
// 0x00000090 System.Boolean UnityStandardAssets.ImageEffects.NoiseAndGrain::CheckResources()
extern void NoiseAndGrain_CheckResources_mB947BDF9C0C6FC33A1D5940907BEB84A019B89FB ();
// 0x00000091 System.Void UnityStandardAssets.ImageEffects.NoiseAndGrain::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void NoiseAndGrain_OnRenderImage_m2F52F777F5149046442DD2C46DF07A0D2F933B70 ();
// 0x00000092 System.Void UnityStandardAssets.ImageEffects.NoiseAndGrain::DrawNoiseQuadGrid(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.Material,UnityEngine.Texture2D,System.Int32)
extern void NoiseAndGrain_DrawNoiseQuadGrid_m0EFDFD6193FE28E1C89123F531785043B7EB2DEF ();
// 0x00000093 System.Void UnityStandardAssets.ImageEffects.NoiseAndGrain::.ctor()
extern void NoiseAndGrain__ctor_m654F493C6EA148F27B5CCA2922AF254ACFE5F031 ();
// 0x00000094 System.Void UnityStandardAssets.ImageEffects.NoiseAndGrain::.cctor()
extern void NoiseAndGrain__cctor_mAB21478387BBD7989A0701C75E2B9B575D652810 ();
// 0x00000095 System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::Start()
extern void NoiseAndScratches_Start_mB26D7047D0FE25388AC0791632291A8762A8E27D ();
// 0x00000096 UnityEngine.Material UnityStandardAssets.ImageEffects.NoiseAndScratches::get_material()
extern void NoiseAndScratches_get_material_m5D3DA308A18072923F3FAC25D5354A72740B017A ();
// 0x00000097 System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::OnDisable()
extern void NoiseAndScratches_OnDisable_m4427691F299B9B907D2D8BFF7645818CEA7E39E4 ();
// 0x00000098 System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::SanitizeParameters()
extern void NoiseAndScratches_SanitizeParameters_m356A329C6C040C5B3072F732F2D82B69E4528A8D ();
// 0x00000099 System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void NoiseAndScratches_OnRenderImage_mB83B89EF20B1884A4F508BB5D20541D43E8F407B ();
// 0x0000009A System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::.ctor()
extern void NoiseAndScratches__ctor_m092532E3B7E1FBFB8AC1FFB77FF0F489BEBFDA79 ();
// 0x0000009B UnityEngine.Material UnityStandardAssets.ImageEffects.PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material)
extern void PostEffectsBase_CheckShaderAndCreateMaterial_m1515D02A58527017FACB2B6AC601B5E67B65C865 ();
// 0x0000009C UnityEngine.Material UnityStandardAssets.ImageEffects.PostEffectsBase::CreateMaterial(UnityEngine.Shader,UnityEngine.Material)
extern void PostEffectsBase_CreateMaterial_mA2EDA33D7CD6FA380975DA46597540AAB17B89AE ();
// 0x0000009D System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::OnEnable()
extern void PostEffectsBase_OnEnable_mFEA4058D703A6A068ECFA899FCCF6CEC8E742967 ();
// 0x0000009E System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckSupport()
extern void PostEffectsBase_CheckSupport_m33F744872944BE0FB9A9FBF5FB73CF1CC62BD012 ();
// 0x0000009F System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckResources()
extern void PostEffectsBase_CheckResources_m31A44EE19F985DCD4D3242F9197BF1435F1C8094 ();
// 0x000000A0 System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::Start()
extern void PostEffectsBase_Start_mA2E9CD553BD5AB2AD1963EC250854408434701F1 ();
// 0x000000A1 System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckSupport(System.Boolean)
extern void PostEffectsBase_CheckSupport_mB308BE6390C0474C92E742A561F90423C1502C04 ();
// 0x000000A2 System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckSupport(System.Boolean,System.Boolean)
extern void PostEffectsBase_CheckSupport_mB672E1075EC2C7E643A512D22CFB3000CC681636 ();
// 0x000000A3 System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::Dx11Support()
extern void PostEffectsBase_Dx11Support_m51594DC020FEA76B63FC6804CAB21D88B8497C75 ();
// 0x000000A4 System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::ReportAutoDisable()
extern void PostEffectsBase_ReportAutoDisable_mEFEF901F4F2DC5EDBC11340F930760EF8B10645C ();
// 0x000000A5 System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckShader(UnityEngine.Shader)
extern void PostEffectsBase_CheckShader_mE87A8B176C2F6264E568AD9EFFE7A64F9057CB43 ();
// 0x000000A6 System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::NotSupported()
extern void PostEffectsBase_NotSupported_mD5139A9AE6103E7BF485626371A61B4C146C035C ();
// 0x000000A7 System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::DrawBorder(UnityEngine.RenderTexture,UnityEngine.Material)
extern void PostEffectsBase_DrawBorder_m3B6891159B6BFFA4621F38DCE1648176C0DC4C2B ();
// 0x000000A8 System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::.ctor()
extern void PostEffectsBase__ctor_m440C9B609EF88230A2EB266FD3E6C624431E1368 ();
// 0x000000A9 System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void PostEffectsHelper_OnRenderImage_mD7B6A5F367C7B06AB3FF0F05104CE20732C213D9 ();
// 0x000000AA System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::DrawLowLevelPlaneAlignedWithCamera(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.Material,UnityEngine.Camera)
extern void PostEffectsHelper_DrawLowLevelPlaneAlignedWithCamera_m3B679EFB965DAB4F4A8820AD7D47129C137DA64D ();
// 0x000000AB System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::DrawBorder(UnityEngine.RenderTexture,UnityEngine.Material)
extern void PostEffectsHelper_DrawBorder_m44CAB1AF05C3AED1744164DC20784E659D64535E ();
// 0x000000AC System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::DrawLowLevelQuad(System.Single,System.Single,System.Single,System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.Material)
extern void PostEffectsHelper_DrawLowLevelQuad_mBEBEAC173A76D40A55FF67FB08B82743847A927C ();
// 0x000000AD System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::.ctor()
extern void PostEffectsHelper__ctor_m34FA76EF2A1D31EF180BCA8C1BC208BF2343E18C ();
// 0x000000AE System.Boolean UnityStandardAssets.ImageEffects.Quads::HasMeshes()
extern void Quads_HasMeshes_mC65A4839334BA8CE48D83F3A413A097948058318 ();
// 0x000000AF System.Void UnityStandardAssets.ImageEffects.Quads::Cleanup()
extern void Quads_Cleanup_mECB9AB3FD57767CB13677C676B56A3B20ED12992 ();
// 0x000000B0 UnityEngine.Mesh[] UnityStandardAssets.ImageEffects.Quads::GetMeshes(System.Int32,System.Int32)
extern void Quads_GetMeshes_m82EE0563272627DC809681B206599583F4520ED8 ();
// 0x000000B1 UnityEngine.Mesh UnityStandardAssets.ImageEffects.Quads::GetMesh(System.Int32,System.Int32,System.Int32,System.Int32)
extern void Quads_GetMesh_mFAEBB463DE02000C2B54265298A949D3E7AC5EAE ();
// 0x000000B2 System.Void UnityStandardAssets.ImageEffects.Quads::.ctor()
extern void Quads__ctor_m546BE0C5638614A041EBE43B9DFCA9F07384CDB0 ();
// 0x000000B3 System.Void UnityStandardAssets.ImageEffects.Quads::.cctor()
extern void Quads__cctor_m64556914C3E944D0CA957DF2EA42702C61AC9332 ();
// 0x000000B4 System.Boolean UnityStandardAssets.ImageEffects.ScreenOverlay::CheckResources()
extern void ScreenOverlay_CheckResources_m57EA65A485D62086E8D09E0021F59BCF9545A922 ();
// 0x000000B5 System.Void UnityStandardAssets.ImageEffects.ScreenOverlay::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ScreenOverlay_OnRenderImage_mEBE59EA720CA61C46BE4A499381BD69A7328D192 ();
// 0x000000B6 System.Void UnityStandardAssets.ImageEffects.ScreenOverlay::.ctor()
extern void ScreenOverlay__ctor_m219B73BF28A9EA7CF73BB7AF605CD993C54BC0F8 ();
// 0x000000B7 System.Boolean UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::CheckResources()
extern void ScreenSpaceAmbientObscurance_CheckResources_m689B7754E9CAA08A5B02A353AFB4951EAC6AF6AC ();
// 0x000000B8 System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::OnDisable()
extern void ScreenSpaceAmbientObscurance_OnDisable_mC2105D6501FAAD50891F59D7CAC4CAF51C1B792B ();
// 0x000000B9 System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ScreenSpaceAmbientObscurance_OnRenderImage_m61032625FA720DD91B91DDCB6536FFBCC30D309D ();
// 0x000000BA System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::.ctor()
extern void ScreenSpaceAmbientObscurance__ctor_m29CC36C4651630397EADF5A0D4A4909B5576F3A0 ();
// 0x000000BB UnityEngine.Material UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::CreateMaterial(UnityEngine.Shader)
extern void ScreenSpaceAmbientOcclusion_CreateMaterial_m1834FBA5AE4C7A8A4CA79509CAC358A38ADBCFE6 ();
// 0x000000BC System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::DestroyMaterial(UnityEngine.Material)
extern void ScreenSpaceAmbientOcclusion_DestroyMaterial_m7544E50BF8F73003D213499FF46E596B8C1B4F50 ();
// 0x000000BD System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::OnDisable()
extern void ScreenSpaceAmbientOcclusion_OnDisable_m721693E9EB31E7A2F08C15D8CD0894D4B823F716 ();
// 0x000000BE System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::Start()
extern void ScreenSpaceAmbientOcclusion_Start_m1571C500A5A278D5DAE2C5DA55EFCB571B349FB7 ();
// 0x000000BF System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::OnEnable()
extern void ScreenSpaceAmbientOcclusion_OnEnable_m198EE5461CE656D45B232392E05A68835F51A999 ();
// 0x000000C0 System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::CreateMaterials()
extern void ScreenSpaceAmbientOcclusion_CreateMaterials_m7F4FA9A74460B79854A938FFD887B17F14654FC3 ();
// 0x000000C1 System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ScreenSpaceAmbientOcclusion_OnRenderImage_mA0314378A64307F2C0E686CAE67AF5888393B1D9 ();
// 0x000000C2 System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::.ctor()
extern void ScreenSpaceAmbientOcclusion__ctor_m218DBE7B9B0B96D338E146BC80E578A0253A9A49 ();
// 0x000000C3 System.Void UnityStandardAssets.ImageEffects.SepiaTone::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void SepiaTone_OnRenderImage_m490AC5259211D03D0E4AE30DE9C2A5F0226522F3 ();
// 0x000000C4 System.Void UnityStandardAssets.ImageEffects.SepiaTone::.ctor()
extern void SepiaTone__ctor_m51763A9838E8620AF414B9CDF3899A046988F93A ();
// 0x000000C5 System.Boolean UnityStandardAssets.ImageEffects.SunShafts::CheckResources()
extern void SunShafts_CheckResources_m04FB75C8E1935601236477B7EBAD9B3C8B36C71E ();
// 0x000000C6 System.Void UnityStandardAssets.ImageEffects.SunShafts::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void SunShafts_OnRenderImage_mD4C66ABED9567F6FD243956E83AC38042DF95438 ();
// 0x000000C7 System.Void UnityStandardAssets.ImageEffects.SunShafts::.ctor()
extern void SunShafts__ctor_mA9BAA6C7A73DC359D17716CAF7F5C43E4D51DE25 ();
// 0x000000C8 System.Boolean UnityStandardAssets.ImageEffects.TiltShift::CheckResources()
extern void TiltShift_CheckResources_m76B02FC4E3A427962D54759550D879984DD529A7 ();
// 0x000000C9 System.Void UnityStandardAssets.ImageEffects.TiltShift::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void TiltShift_OnRenderImage_mBDF4A022973B6AF25D4162001ECBEB0613FACF93 ();
// 0x000000CA System.Void UnityStandardAssets.ImageEffects.TiltShift::.ctor()
extern void TiltShift__ctor_m8936621435FAB038B98B98CA7282E32106BE1EA7 ();
// 0x000000CB System.Boolean UnityStandardAssets.ImageEffects.Tonemapping::CheckResources()
extern void Tonemapping_CheckResources_mF10047DA83F89CF8C93C7A263245A501404DBB95 ();
// 0x000000CC System.Single UnityStandardAssets.ImageEffects.Tonemapping::UpdateCurve()
extern void Tonemapping_UpdateCurve_m3ACD656292CF96ECD6A1F684554D94D44EFDF1C7 ();
// 0x000000CD System.Void UnityStandardAssets.ImageEffects.Tonemapping::OnDisable()
extern void Tonemapping_OnDisable_m39886645752B0190BBB4A5643C3941E57C591677 ();
// 0x000000CE System.Boolean UnityStandardAssets.ImageEffects.Tonemapping::CreateInternalRenderTexture()
extern void Tonemapping_CreateInternalRenderTexture_m57DA5B1E686F03D4C4E99D4920DBC5AFAF1835AF ();
// 0x000000CF System.Void UnityStandardAssets.ImageEffects.Tonemapping::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Tonemapping_OnRenderImage_mC820173AF7A5683200AC7D0CA2A68FB940BDBAA0 ();
// 0x000000D0 System.Void UnityStandardAssets.ImageEffects.Tonemapping::.ctor()
extern void Tonemapping__ctor_m37B1A6CFAB1CBD05E8FB154AAED049B7FEB40E01 ();
// 0x000000D1 System.Boolean UnityStandardAssets.ImageEffects.Triangles::HasMeshes()
extern void Triangles_HasMeshes_m1AB519C58CAF720EAC124D2EAA032E860C991717 ();
// 0x000000D2 System.Void UnityStandardAssets.ImageEffects.Triangles::Cleanup()
extern void Triangles_Cleanup_m2279FD4ECA2AA9BEACEC30E14CE376298D5A4254 ();
// 0x000000D3 UnityEngine.Mesh[] UnityStandardAssets.ImageEffects.Triangles::GetMeshes(System.Int32,System.Int32)
extern void Triangles_GetMeshes_mFDEE8E3D19D4285F625F244F8FB0C04E5BEE6E04 ();
// 0x000000D4 UnityEngine.Mesh UnityStandardAssets.ImageEffects.Triangles::GetMesh(System.Int32,System.Int32,System.Int32,System.Int32)
extern void Triangles_GetMesh_m5523727AC3B619665D6F24679B5127CD9678DC59 ();
// 0x000000D5 System.Void UnityStandardAssets.ImageEffects.Triangles::.ctor()
extern void Triangles__ctor_m970429AD51520FE2B9B2984A2024628AAA94706A ();
// 0x000000D6 System.Void UnityStandardAssets.ImageEffects.Triangles::.cctor()
extern void Triangles__cctor_mE20F5C7B1B26C5F7BCD9F13FD19245823F4570A5 ();
// 0x000000D7 System.Void UnityStandardAssets.ImageEffects.Twirl::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Twirl_OnRenderImage_mBC71C83543FB82E48C76A7925192F2A5076F44DB ();
// 0x000000D8 System.Void UnityStandardAssets.ImageEffects.Twirl::.ctor()
extern void Twirl__ctor_m276BA32AEDE57F4E388B2DC82E8BE572BC2C0329 ();
// 0x000000D9 System.Boolean UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::CheckResources()
extern void VignetteAndChromaticAberration_CheckResources_m8FD9076266E9E792ADBE50A35604F28F88687E69 ();
// 0x000000DA System.Void UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void VignetteAndChromaticAberration_OnRenderImage_m058FA072700D220C0DB624A8FC76A29CD144CAC5 ();
// 0x000000DB System.Void UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::.ctor()
extern void VignetteAndChromaticAberration__ctor_m37B02A6651BAEBC09BCDC51A7A81953A3CE13BD6 ();
// 0x000000DC System.Void UnityStandardAssets.ImageEffects.Vortex::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Vortex_OnRenderImage_m6FE1458882E5F47371016A339143B443CA44A61F ();
// 0x000000DD System.Void UnityStandardAssets.ImageEffects.Vortex::.ctor()
extern void Vortex__ctor_m9E271B34D1441B5E030C40E972DE1DB16E9ED673 ();
// 0x000000DE System.Void Try_<GameOver>d__9::.ctor(System.Int32)
extern void U3CGameOverU3Ed__9__ctor_m219562B912D2DAEDF67FE69BE25121046AA04298 ();
// 0x000000DF System.Void Try_<GameOver>d__9::System.IDisposable.Dispose()
extern void U3CGameOverU3Ed__9_System_IDisposable_Dispose_mF6FA43948B8E54341163D816E9584F1C77E93902 ();
// 0x000000E0 System.Boolean Try_<GameOver>d__9::MoveNext()
extern void U3CGameOverU3Ed__9_MoveNext_mCC3B8C83E8F872724817F1E264C1441E7CABD08A ();
// 0x000000E1 System.Object Try_<GameOver>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGameOverU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7D6B3A3CC6F4EA7D8CE1BA9ED9B59E7A0862FA91 ();
// 0x000000E2 System.Void Try_<GameOver>d__9::System.Collections.IEnumerator.Reset()
extern void U3CGameOverU3Ed__9_System_Collections_IEnumerator_Reset_mCA029110A783F667BCC60439CCDCBA58CF09FB0B ();
// 0x000000E3 System.Object Try_<GameOver>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CGameOverU3Ed__9_System_Collections_IEnumerator_get_Current_m2BED9C5F9D0CF5851C030A915DC617D22B069571 ();
static Il2CppMethodPointer s_methodPointers[227] = 
{
	Try_Start_m77D9F45762D9F92B629EA62238C835B4CB6E78F8,
	Try_Update_m0A0CDCEC34F921E403FAE59FEC4F74FAB60B75A2,
	Try_GameOver_m63B4882E6F5350C69D3A60375E0F37A9D886FCC8,
	Try__ctor_m17D029E7B5721BFF27701E579B38EA87F6EBBACF,
	SurfaceChecker_Start_m7400B2EE1A899C91E301778ABD13C1DC8028BFE7,
	SurfaceChecker_OnGUI_mBBFBD99AB32C1C9D01F60AF73AC3A1A9AE7F99B5,
	SurfaceChecker_Update_m94C00EF9477B72AD1B162B3F6ADAEC80AD57E496,
	SurfaceChecker_UpdateTargetIndicator_mCAB90E3DFA3B954FB764ECA1C599EF1CCA7FD3CA,
	SurfaceChecker_UpdatePlacementPose_m5530A5B7DD542CE0783ECEDDF5CCB60FCDA96CBC,
	SurfaceChecker__ctor_mBCC8B256BABABC33A19219B00A57100E44531F11,
	Antialiasing_CurrentAAMaterial_mDCAB4F1E05EDD77DE4019689E9B8E0D8C83A8C54,
	Antialiasing_CheckResources_m498B9EE12AFAD27688FAC7173EEB43F81017FB88,
	Antialiasing_OnRenderImage_mA976E0852A74E97648A82B6BD10E66ACC1AF7FD4,
	Antialiasing__ctor_m55C3D8A38ACC338A1FE308C1DE62C63975924F84,
	Bloom_CheckResources_m92106259EA26543B36D042032DE604E103FA6D68,
	Bloom_OnRenderImage_mB9448277B070C65684A4752B979638AF435DB106,
	Bloom_AddTo_m2C7566CB79AD6CB39439004E35DE6F89C42621F4,
	Bloom_BlendFlares_mBE0265621414D22718647708DE1471D19D17C9B9,
	Bloom_BrightFilter_m0B93F00834C7B463AF74F9A5D755D2647A1F96A4,
	Bloom_BrightFilter_m30BA24087FB56D24159743A86EE8153B6E08A576,
	Bloom_Vignette_m05374CF6C79C9435CDB9915669D3E6CCCAE91783,
	Bloom__ctor_mDBE9F226A2C05547E468A1095EC0CC57DBCD6C27,
	BloomAndFlares_CheckResources_m16DCC48C4E67BCB0BE3219A77CAA1454E6CEBAD9,
	BloomAndFlares_OnRenderImage_m801989E7A9037FC9EE826E2EB363EE42578EDC0A,
	BloomAndFlares_AddTo_m00E0AC64F23F18A9EF9D6CDF8BBE3C82ABABF853,
	BloomAndFlares_BlendFlares_mE4C7FC30CA32888A348B4490305DC8A750DDF442,
	BloomAndFlares_BrightFilter_mBA4DE2DFD592941DEBC1CF737EBA45F1E5093A15,
	BloomAndFlares_Vignette_mCCAD05854B3AF95FCEFE32022170EEBE66D03766,
	BloomAndFlares__ctor_mAB0F2AD4210DC5BCCDAD71FEE375DD4ACC48B7ED,
	BloomOptimized_CheckResources_mD632E635CB25AD6B910137EACA4B47413782B297,
	BloomOptimized_OnDisable_m6C76582A668E9F0AD13481E54FF47AFBF5FE7EFE,
	BloomOptimized_OnRenderImage_m8ACD073311ECEB136FF61084DD541125C69F2C42,
	BloomOptimized__ctor_m9A393CAC94F698AC6DFD0407C439C885DF249189,
	Blur_get_material_mC339DD88FD2BC429680CA2F25C8BED5F80735B79,
	Blur_OnDisable_m709199A9983BE2C2283D6DEF0980653036951F10,
	Blur_Start_m6C503845B602898F68A18E5A0D6D7712FD818586,
	Blur_FourTapCone_m651F9E47B8AB1958902785D9507F6F46130AC8B3,
	Blur_DownSample4x_mF95A54B819BAC3B7140F3CAE39EC1F7B0FBF4776,
	Blur_OnRenderImage_m371A7A264E94739EFEED8EE1CB1CFC9F9F04DEC7,
	Blur__ctor_m85C8C095B8C3B506E20B36B7E1838B4B06AC93E0,
	Blur__cctor_mD80C6694F31CC7E5A18C9A8ABC9209782F309189,
	BlurOptimized_CheckResources_m33D49532FE335BEC1B842D4C1016473B1EBE7CB1,
	BlurOptimized_OnDisable_m2FFA9FFB9791D797603B589E7C9F9A164A1B5BE3,
	BlurOptimized_OnRenderImage_m1C441E7D77F6D3B77C9C4BD49D14855AD352B3A9,
	BlurOptimized__ctor_mF83F3B51DD987C09E5BE74EC65795FC89B51F019,
	CameraMotionBlur_CalculateViewProjection_mACC204367238A287CC7A38C01394C82CE5EDED87,
	CameraMotionBlur_Start_m97790DE300FC791125EA136645CE2D77EB73F4AA,
	CameraMotionBlur_OnEnable_m08997525FECCD04035EF42F33919941BE139DC03,
	CameraMotionBlur_OnDisable_m1DF62D2CAB4995F38C23B1966F5EF47C7340CF08,
	CameraMotionBlur_CheckResources_m8D026DBB9258C285D2FF092F44147C337B245025,
	CameraMotionBlur_OnRenderImage_m825CD100D4C2A4262BFB96E1461748B49A1DAF5D,
	CameraMotionBlur_Remember_mF7924AB7BF552DA1568B8671E7E7DAE57073EAA8,
	CameraMotionBlur_GetTmpCam_mFC1B30B16B7CD31C659E7660DF7928E9183ED7E2,
	CameraMotionBlur_StartFrame_m7D9B86914F003077FBD6BA88D5488BA163EBFFDB,
	CameraMotionBlur_divRoundUp_m920A99D3CFC242852027F0560968681FA722CBDD,
	CameraMotionBlur__ctor_m63EB59EE479795A61C8BDC7817338590FC944017,
	CameraMotionBlur__cctor_m72319853F4B6038AED29E727CABADA3F81A83DA3,
	ColorCorrectionCurves_Start_m96C795F6E1B3E3F2969590C1842338D9DE4336CC,
	ColorCorrectionCurves_Awake_m611F8DC335D42F19A9F5BE775AE622069C80F8F6,
	ColorCorrectionCurves_CheckResources_mE8B24D1F01C6E1EA879816FBB17C203CC33FB775,
	ColorCorrectionCurves_UpdateParameters_m4849CC559E048FD7E3E7FAF83DBBDFB019D54415,
	ColorCorrectionCurves_UpdateTextures_m05A13699BF3A502399C23D86C00782D8033BD7BC,
	ColorCorrectionCurves_OnRenderImage_m1B8C7B3ADBE01FA66853467EB0B07A563A5F8352,
	ColorCorrectionCurves__ctor_m68722ED5CF2EB526B5F02A93D233B9D95F2C6D97,
	ColorCorrectionLookup_CheckResources_m667A5A869CC0206C1E33640EC4E76A5E03078721,
	ColorCorrectionLookup_OnDisable_mF1F9FD81B997F5C64C76D6EA4D9FFAAD778752DC,
	ColorCorrectionLookup_OnDestroy_mB168904FCBE7B6038896284D55FFC7C94A1BA182,
	ColorCorrectionLookup_SetIdentityLut_mDEB155C0810472446CB5DB837AEB24294F981C09,
	ColorCorrectionLookup_ValidDimensions_mFBAEDC77ED3A2937C618CBE36B374A213523147E,
	ColorCorrectionLookup_Convert_mBEE45AD4A6BFCD6B311016298F9D5B6F2B6FEFC2,
	ColorCorrectionLookup_OnRenderImage_m9CCFEE4FD27332D235C59239FE2A286728CC9A52,
	ColorCorrectionLookup__ctor_m0DFF254050E4CEB608FCA1462A9587A227973D9F,
	ColorCorrectionRamp_OnRenderImage_m525E7EC26C5C858DE4A14C7BB82A743343D94B84,
	ColorCorrectionRamp__ctor_m3943A74E178F836266D12D363F943566C0AF2779,
	ContrastEnhance_CheckResources_m285591510F4D995B7BD926A306BCD69485A26EAD,
	ContrastEnhance_OnRenderImage_m194287EACB23A9B4561D8B47D9BE2B92B33BE38E,
	ContrastEnhance__ctor_m03223D280A37A44AF64FB8D5368B8A652BE1DE25,
	ContrastStretch_get_materialLum_m3B94D8370783820BF61ADEA97169CD858D12BD0A,
	ContrastStretch_get_materialReduce_mFFAEB3EC39637918599727612C933A794D5B482F,
	ContrastStretch_get_materialAdapt_m95A164030016587300035F3DE58087F5F24ADF50,
	ContrastStretch_get_materialApply_mD9802FD06F513EC1BD1D20F20E52242150EAFEEC,
	ContrastStretch_Start_mE367825F381B6813C8766323829735A6279597CD,
	ContrastStretch_OnEnable_mDD7FECD219EAC6A7075290DF68F21862388E4718,
	ContrastStretch_OnDisable_mD742FF9882E9D9060B8967269C87F9F978D059F5,
	ContrastStretch_OnRenderImage_m1CF0C6BACB625EE274D813A9181925A3E31FC649,
	ContrastStretch_CalculateAdaptation_m037490E256613C7D4BA7E209A2632CCE403B2B12,
	ContrastStretch__ctor_m59166DE11011E5DBBD366B2B594F67198508EC07,
	CreaseShading_CheckResources_m75335C51F99C80FB15EC8CA2CE8C17D5D7CCDCE0,
	CreaseShading_OnRenderImage_mA734C73D6E2BD430F9156A34FEFF7C3E0EC261B2,
	CreaseShading__ctor_m5F1EA0C9E5F16FBDD152AE723066DCCCD7513906,
	DepthOfField_CheckResources_m99902E019852BA913338849C829238FE94723076,
	DepthOfField_OnEnable_m4CCE526BA0FC56B2B74F88640DF4A8CBC556637D,
	DepthOfField_OnDisable_mDCFFA4DC1A2119AC21F2287D500869B78A570291,
	DepthOfField_ReleaseComputeResources_mE9C0CFAA5296947EE0FD7F2EF077DAFAD304C969,
	DepthOfField_CreateComputeResources_mE8EFBE513F52E734A7EF32FCDFB9CFE8995577DD,
	DepthOfField_FocalDistance01_m7C83CD9C716FD024B93ADDF8104C3FB7155C35DD,
	DepthOfField_WriteCoc_mD5EEEB880C4EBFF74E2512EC41FCBF2DE09E1AA8,
	DepthOfField_OnRenderImage_mCCAF5027A0E4720B7EB307F6775FD481B2882A61,
	DepthOfField__ctor_mAAD3FC97724C123B194DE9AB78F1534C6E41D8C9,
	DepthOfFieldDeprecated_CreateMaterials_mECB51EE5BD0A165CD7857C928FDE637F592C839C,
	DepthOfFieldDeprecated_CheckResources_mAF4DCDB80B5DE79A0BFD6B487F6A94CA9E20942C,
	DepthOfFieldDeprecated_OnDisable_mE788E8B945FEBAB0D4B976595FEBD2A0615CA066,
	DepthOfFieldDeprecated_OnEnable_mA937196855E09E213A14AC82ECD6BBA36204B6A3,
	DepthOfFieldDeprecated_FocalDistance01_m813C7BA00F70EE2499BA9FF010F50315C0358F78,
	DepthOfFieldDeprecated_GetDividerBasedOnQuality_mAEA18B66A910E404DCB26D56A764001B526E0EDB,
	DepthOfFieldDeprecated_GetLowResolutionDividerBasedOnQuality_m07162EC127851FF5B26F81A300F2628269A4C899,
	DepthOfFieldDeprecated_OnRenderImage_mB105D9596DD852280DB427C68FD74E2FB5C9D406,
	DepthOfFieldDeprecated_Blur_mAD0A3BA1246C5D4B728983DC1D6AE4379CA3343B,
	DepthOfFieldDeprecated_BlurFg_mB89E6D910AB144124AA390F592555F333F6FF238,
	DepthOfFieldDeprecated_BlurHex_m92FCA492A8C5B533079254B451E65CC838DCA5B7,
	DepthOfFieldDeprecated_Downsample_mA34F5C964DF9DCA387288AD6860799AEF5ACE3C4,
	DepthOfFieldDeprecated_AddBokeh_m17F24DBB48B9E3F821DB6D0E8AE5BA732FFD8A53,
	DepthOfFieldDeprecated_ReleaseTextures_m2CB5AC6D29EB2CCB7480BAEB97F80A9046EFB819,
	DepthOfFieldDeprecated_AllocateTextures_mD948CE56E5DEC1CF0E51120DF9EFB354321E30C0,
	DepthOfFieldDeprecated__ctor_m797E5E012F35EC7DFE7E5A9CE8C5D7A99E4A077F,
	DepthOfFieldDeprecated__cctor_m7A951BF59CEAE1D7C1FA676A246A3D46B2A0A935,
	EdgeDetection_CheckResources_m38B9BC21E5FBA2716D9319443277216191BFAC98,
	EdgeDetection_Start_m242E5FB7F37327A2447953EEBF59A85451D8BCCC,
	EdgeDetection_SetCameraFlag_m2C438A1B4D88F41D69D5038E9230AB99FF5FA41E,
	EdgeDetection_OnEnable_mAD697941029C90030AC0CF02FB7EA0DBBED6887A,
	EdgeDetection_OnRenderImage_m48F01278CA34F14B17A169715DAC376B3D3D7189,
	EdgeDetection__ctor_m00052AE0C25B0BDD68C10929C7484FA85B857521,
	Fisheye_CheckResources_m72A552D5AF92C69A59DD94A406660283BBC4CFDD,
	Fisheye_OnRenderImage_m57F4D4A413BD705977FA0E5776B7C43B7908CCAF,
	Fisheye__ctor_mBC52733545C135CE081043C8ED60ECEAAD941193,
	GlobalFog_CheckResources_m4D2FEA4A379EBA867C51D442AAC297F07D6C1D35,
	GlobalFog_OnRenderImage_mE92A91E8A011F3522CBC70D6771BEC588C4B10DB,
	GlobalFog_CustomGraphicsBlit_m4D20CB76C88420AF37C959A360BF2EB8BC34DF71,
	GlobalFog__ctor_m0A9DCB22E188DFE08579AD41764831F203F730FA,
	Grayscale_OnRenderImage_m8A512F145218A68A55C64B84B23E2131C269A24A,
	Grayscale__ctor_m73FB63226C982A7360275EDB1B7C9CBECFFD5A92,
	ImageEffectBase_Start_mA1BA9EAFE8C4DB8D60AF9214354FA12B518C670F,
	ImageEffectBase_get_material_mBBECA2FA70056036BB0A8C8EE12C005B5F540F32,
	ImageEffectBase_OnDisable_m660A3E17A229AEC9D91618C2CB632FEB5EB9428C,
	ImageEffectBase__ctor_m4A9D6AD3B56675C66BDFD7814F74661E8B567FEE,
	ImageEffects_RenderDistortion_mEAD2A6D8B211C9B7EBE054F227AC22FA525AE2DF,
	ImageEffects_Blit_mA6422E3EF42C14DD0012423410D5E6C23681A2A8,
	ImageEffects_BlitWithMaterial_mEB735C49FC9EBE60B95667B5C116A2ED5997E7A7,
	ImageEffects__ctor_mE1A4398899DA8975E63366AB56CA498DE82E16BF,
	MotionBlur_Start_m82B9C590D5312C265F3F59D6CF8A9F39CE46F0E4,
	MotionBlur_OnDisable_m06EA52C7FCBF0FB355978CD5489FB0AC60F45DF3,
	MotionBlur_OnRenderImage_m24D6DCD45A81C6B2EA33E700BEF2E4F485B31CD4,
	MotionBlur__ctor_m69F5FB6BF9026F8D10E338A3EA9F2C63C57CF605,
	NoiseAndGrain_CheckResources_mB947BDF9C0C6FC33A1D5940907BEB84A019B89FB,
	NoiseAndGrain_OnRenderImage_m2F52F777F5149046442DD2C46DF07A0D2F933B70,
	NoiseAndGrain_DrawNoiseQuadGrid_m0EFDFD6193FE28E1C89123F531785043B7EB2DEF,
	NoiseAndGrain__ctor_m654F493C6EA148F27B5CCA2922AF254ACFE5F031,
	NoiseAndGrain__cctor_mAB21478387BBD7989A0701C75E2B9B575D652810,
	NoiseAndScratches_Start_mB26D7047D0FE25388AC0791632291A8762A8E27D,
	NoiseAndScratches_get_material_m5D3DA308A18072923F3FAC25D5354A72740B017A,
	NoiseAndScratches_OnDisable_m4427691F299B9B907D2D8BFF7645818CEA7E39E4,
	NoiseAndScratches_SanitizeParameters_m356A329C6C040C5B3072F732F2D82B69E4528A8D,
	NoiseAndScratches_OnRenderImage_mB83B89EF20B1884A4F508BB5D20541D43E8F407B,
	NoiseAndScratches__ctor_m092532E3B7E1FBFB8AC1FFB77FF0F489BEBFDA79,
	PostEffectsBase_CheckShaderAndCreateMaterial_m1515D02A58527017FACB2B6AC601B5E67B65C865,
	PostEffectsBase_CreateMaterial_mA2EDA33D7CD6FA380975DA46597540AAB17B89AE,
	PostEffectsBase_OnEnable_mFEA4058D703A6A068ECFA899FCCF6CEC8E742967,
	PostEffectsBase_CheckSupport_m33F744872944BE0FB9A9FBF5FB73CF1CC62BD012,
	PostEffectsBase_CheckResources_m31A44EE19F985DCD4D3242F9197BF1435F1C8094,
	PostEffectsBase_Start_mA2E9CD553BD5AB2AD1963EC250854408434701F1,
	PostEffectsBase_CheckSupport_mB308BE6390C0474C92E742A561F90423C1502C04,
	PostEffectsBase_CheckSupport_mB672E1075EC2C7E643A512D22CFB3000CC681636,
	PostEffectsBase_Dx11Support_m51594DC020FEA76B63FC6804CAB21D88B8497C75,
	PostEffectsBase_ReportAutoDisable_mEFEF901F4F2DC5EDBC11340F930760EF8B10645C,
	PostEffectsBase_CheckShader_mE87A8B176C2F6264E568AD9EFFE7A64F9057CB43,
	PostEffectsBase_NotSupported_mD5139A9AE6103E7BF485626371A61B4C146C035C,
	PostEffectsBase_DrawBorder_m3B6891159B6BFFA4621F38DCE1648176C0DC4C2B,
	PostEffectsBase__ctor_m440C9B609EF88230A2EB266FD3E6C624431E1368,
	PostEffectsHelper_OnRenderImage_mD7B6A5F367C7B06AB3FF0F05104CE20732C213D9,
	PostEffectsHelper_DrawLowLevelPlaneAlignedWithCamera_m3B679EFB965DAB4F4A8820AD7D47129C137DA64D,
	PostEffectsHelper_DrawBorder_m44CAB1AF05C3AED1744164DC20784E659D64535E,
	PostEffectsHelper_DrawLowLevelQuad_mBEBEAC173A76D40A55FF67FB08B82743847A927C,
	PostEffectsHelper__ctor_m34FA76EF2A1D31EF180BCA8C1BC208BF2343E18C,
	Quads_HasMeshes_mC65A4839334BA8CE48D83F3A413A097948058318,
	Quads_Cleanup_mECB9AB3FD57767CB13677C676B56A3B20ED12992,
	Quads_GetMeshes_m82EE0563272627DC809681B206599583F4520ED8,
	Quads_GetMesh_mFAEBB463DE02000C2B54265298A949D3E7AC5EAE,
	Quads__ctor_m546BE0C5638614A041EBE43B9DFCA9F07384CDB0,
	Quads__cctor_m64556914C3E944D0CA957DF2EA42702C61AC9332,
	ScreenOverlay_CheckResources_m57EA65A485D62086E8D09E0021F59BCF9545A922,
	ScreenOverlay_OnRenderImage_mEBE59EA720CA61C46BE4A499381BD69A7328D192,
	ScreenOverlay__ctor_m219B73BF28A9EA7CF73BB7AF605CD993C54BC0F8,
	ScreenSpaceAmbientObscurance_CheckResources_m689B7754E9CAA08A5B02A353AFB4951EAC6AF6AC,
	ScreenSpaceAmbientObscurance_OnDisable_mC2105D6501FAAD50891F59D7CAC4CAF51C1B792B,
	ScreenSpaceAmbientObscurance_OnRenderImage_m61032625FA720DD91B91DDCB6536FFBCC30D309D,
	ScreenSpaceAmbientObscurance__ctor_m29CC36C4651630397EADF5A0D4A4909B5576F3A0,
	ScreenSpaceAmbientOcclusion_CreateMaterial_m1834FBA5AE4C7A8A4CA79509CAC358A38ADBCFE6,
	ScreenSpaceAmbientOcclusion_DestroyMaterial_m7544E50BF8F73003D213499FF46E596B8C1B4F50,
	ScreenSpaceAmbientOcclusion_OnDisable_m721693E9EB31E7A2F08C15D8CD0894D4B823F716,
	ScreenSpaceAmbientOcclusion_Start_m1571C500A5A278D5DAE2C5DA55EFCB571B349FB7,
	ScreenSpaceAmbientOcclusion_OnEnable_m198EE5461CE656D45B232392E05A68835F51A999,
	ScreenSpaceAmbientOcclusion_CreateMaterials_m7F4FA9A74460B79854A938FFD887B17F14654FC3,
	ScreenSpaceAmbientOcclusion_OnRenderImage_mA0314378A64307F2C0E686CAE67AF5888393B1D9,
	ScreenSpaceAmbientOcclusion__ctor_m218DBE7B9B0B96D338E146BC80E578A0253A9A49,
	SepiaTone_OnRenderImage_m490AC5259211D03D0E4AE30DE9C2A5F0226522F3,
	SepiaTone__ctor_m51763A9838E8620AF414B9CDF3899A046988F93A,
	SunShafts_CheckResources_m04FB75C8E1935601236477B7EBAD9B3C8B36C71E,
	SunShafts_OnRenderImage_mD4C66ABED9567F6FD243956E83AC38042DF95438,
	SunShafts__ctor_mA9BAA6C7A73DC359D17716CAF7F5C43E4D51DE25,
	TiltShift_CheckResources_m76B02FC4E3A427962D54759550D879984DD529A7,
	TiltShift_OnRenderImage_mBDF4A022973B6AF25D4162001ECBEB0613FACF93,
	TiltShift__ctor_m8936621435FAB038B98B98CA7282E32106BE1EA7,
	Tonemapping_CheckResources_mF10047DA83F89CF8C93C7A263245A501404DBB95,
	Tonemapping_UpdateCurve_m3ACD656292CF96ECD6A1F684554D94D44EFDF1C7,
	Tonemapping_OnDisable_m39886645752B0190BBB4A5643C3941E57C591677,
	Tonemapping_CreateInternalRenderTexture_m57DA5B1E686F03D4C4E99D4920DBC5AFAF1835AF,
	Tonemapping_OnRenderImage_mC820173AF7A5683200AC7D0CA2A68FB940BDBAA0,
	Tonemapping__ctor_m37B1A6CFAB1CBD05E8FB154AAED049B7FEB40E01,
	Triangles_HasMeshes_m1AB519C58CAF720EAC124D2EAA032E860C991717,
	Triangles_Cleanup_m2279FD4ECA2AA9BEACEC30E14CE376298D5A4254,
	Triangles_GetMeshes_mFDEE8E3D19D4285F625F244F8FB0C04E5BEE6E04,
	Triangles_GetMesh_m5523727AC3B619665D6F24679B5127CD9678DC59,
	Triangles__ctor_m970429AD51520FE2B9B2984A2024628AAA94706A,
	Triangles__cctor_mE20F5C7B1B26C5F7BCD9F13FD19245823F4570A5,
	Twirl_OnRenderImage_mBC71C83543FB82E48C76A7925192F2A5076F44DB,
	Twirl__ctor_m276BA32AEDE57F4E388B2DC82E8BE572BC2C0329,
	VignetteAndChromaticAberration_CheckResources_m8FD9076266E9E792ADBE50A35604F28F88687E69,
	VignetteAndChromaticAberration_OnRenderImage_m058FA072700D220C0DB624A8FC76A29CD144CAC5,
	VignetteAndChromaticAberration__ctor_m37B02A6651BAEBC09BCDC51A7A81953A3CE13BD6,
	Vortex_OnRenderImage_m6FE1458882E5F47371016A339143B443CA44A61F,
	Vortex__ctor_m9E271B34D1441B5E030C40E972DE1DB16E9ED673,
	U3CGameOverU3Ed__9__ctor_m219562B912D2DAEDF67FE69BE25121046AA04298,
	U3CGameOverU3Ed__9_System_IDisposable_Dispose_mF6FA43948B8E54341163D816E9584F1C77E93902,
	U3CGameOverU3Ed__9_MoveNext_mCC3B8C83E8F872724817F1E264C1441E7CABD08A,
	U3CGameOverU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7D6B3A3CC6F4EA7D8CE1BA9ED9B59E7A0862FA91,
	U3CGameOverU3Ed__9_System_Collections_IEnumerator_Reset_mCA029110A783F667BCC60439CCDCBA58CF09FB0B,
	U3CGameOverU3Ed__9_System_Collections_IEnumerator_get_Current_m2BED9C5F9D0CF5851C030A915DC617D22B069571,
};
static const int32_t s_InvokerIndices[227] = 
{
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	114,
	27,
	23,
	114,
	27,
	1979,
	27,
	1979,
	1980,
	1979,
	23,
	114,
	27,
	1979,
	27,
	1981,
	1979,
	23,
	114,
	23,
	27,
	23,
	14,
	23,
	23,
	598,
	27,
	27,
	23,
	3,
	114,
	23,
	27,
	23,
	23,
	23,
	23,
	23,
	114,
	27,
	23,
	14,
	23,
	137,
	23,
	3,
	23,
	23,
	114,
	23,
	23,
	27,
	23,
	114,
	23,
	23,
	23,
	9,
	27,
	27,
	23,
	27,
	23,
	114,
	27,
	23,
	14,
	14,
	14,
	14,
	23,
	23,
	23,
	27,
	26,
	23,
	114,
	27,
	23,
	114,
	23,
	23,
	23,
	23,
	1087,
	399,
	27,
	23,
	23,
	114,
	23,
	23,
	1087,
	10,
	37,
	27,
	1982,
	1982,
	1983,
	27,
	168,
	23,
	1984,
	23,
	3,
	114,
	23,
	23,
	23,
	27,
	23,
	114,
	27,
	23,
	114,
	27,
	1169,
	23,
	27,
	23,
	23,
	14,
	23,
	23,
	1985,
	134,
	156,
	23,
	23,
	23,
	27,
	23,
	114,
	27,
	641,
	23,
	3,
	23,
	14,
	23,
	23,
	27,
	23,
	113,
	113,
	23,
	114,
	114,
	23,
	187,
	609,
	114,
	23,
	9,
	23,
	27,
	23,
	27,
	1986,
	134,
	1987,
	23,
	49,
	3,
	530,
	1239,
	23,
	3,
	114,
	27,
	23,
	114,
	23,
	27,
	23,
	0,
	122,
	23,
	23,
	23,
	23,
	27,
	23,
	27,
	23,
	114,
	27,
	23,
	114,
	27,
	23,
	114,
	677,
	23,
	114,
	27,
	23,
	49,
	3,
	530,
	1239,
	23,
	3,
	27,
	23,
	114,
	27,
	23,
	27,
	23,
	32,
	23,
	114,
	14,
	23,
	14,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	227,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
